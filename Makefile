# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thaumas <thaumas@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/07/29 18:52:01 by thaumas           #+#    #+#              #
#    Updated: 2020/08/14 21:08:29 by thaumas          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIBASM_DIR = srcs

LIBASM_ARGS = -f elf64

TESTER_ARGS = -c

LIBASM_SRCS = $(LIBASM_DIR)/ft__strlen.s \
				$(LIBASM_DIR)/ft__strcpy.s \
				$(LIBASM_DIR)/ft__strcmp.s \
				$(LIBASM_DIR)/ft__read.s \
				$(LIBASM_DIR)/ft__write.s \
				$(LIBASM_DIR)/ft__strdup.s

TESTER_SRCS = main.c

LIBASM_OBJS = $(LIBASM_SRCS:.s=.o)

TESTER_OBJS := $(TESTER_SRCS:.c=.o)

NAME = libasm.a

TEST_NAME = test

.s.o :
		nasm $(LIBASM_ARGS) $< -o $(<:.s=.o)

.c.o :
		clang $(TESTER_ARGS) $< -o $(<:.c=.o)

all : $(NAME)

$(NAME) : $(LIBASM_OBJS)
		ar rcs $(NAME) $(LIBASM_OBJS)

test : all $(TESTER_OBJS)
		clang -o $(TEST_NAME) $(TESTER_OBJS) -L. -lasm
		@echo "\n\t Now Running the tests ... \n"
		@./$(TEST_NAME)

clean :
		rm -f $(LIBASM_DIR)/*.o
		rm -f main.o

fclean : clean
		rm -f $(NAME)
		rm -f $(TEST_NAME)

re :	fclean all

.PHONY : re all clean fclean