/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thaumas <thaumas@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 17:10:01 by thaumas           #+#    #+#             */
/*   Updated: 2020/08/25 08:51:40 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "srcs/libasm.h"
#include <fcntl.h>


void	test_ft__strlen();

void test_ft__strcpy();

void test_ft__strdup();

void test_ft__strcmp();

void test_ft__read();

void test_ft__write();

int		main(void)
{
	test_ft__strlen();
	test_ft__strcpy();
	test_ft__strdup();
	test_ft__strcmp();
	test_ft__read();
	test_ft__write();
	return 0;
}

void	ft_putstr_fd(const char *str, int fd)
{
	write(fd, str, ft__strlen(str));
}

void	test_ft__strlen(void)
{
	char	str[] = "Salut !";
	char	str2[] = "";
	char	str3[] = "gdfmdfgfjgfd;:;\t\n\\:!;r;ze;lr!rtz#";

	printf("\n>>\tNow Testing\033[32;1m ft__strlen()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	printf("%zu\t|\t%zu\n", strlen(str), ft__strlen(str));
	printf("%zu\t|\t%zu\n", strlen(str2), ft__strlen(str2));
	printf("%zu\t|\t%zu\n", strlen(str3), ft__strlen(str3));
}

void	test_ft__strcpy(void)
{
	char	dest[] = "\0\0\0\0\0";
	char	dest2[] = "\0\0\0\0\0";
	char	dest3[40] = "";
	char	dest4[40] = "";
	char	src[] = "Salut";
	char	src2[] = ":)";
	char	src3[] = "gdfmdfghdfjgfd;:;\t\32n\\:!;r;ze;lr!rtz#";

	printf("\n>>\tNow Testing\033[32;1m ft__strcpy()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	printf("%s\t|\t%s\n", strcpy(dest, src), ft__strcpy(dest2, src));
	printf("%s\t|\t%s\n", strcpy(dest, src2), ft__strcpy(dest2, src2));
	printf("%s\t|\t%s\n", strcpy(dest3, src3), ft__strcpy(dest4, src3));
}

void	test_ft__strdup(void)
{
	char	src[] = "こにちわ";
	char	src2[] = "";
	char	*dest;
	char	*dest2;

	printf("\n>>\tNow Testing\033[32;1m ft__strdup()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	printf("%s\t|\t%s\n", (dest = strdup(src)), (dest2 = ft__strdup(src)));
	free(dest);
	free(dest2);
	printf("%s\t|\t%s\n", (dest = strdup(src2)), (dest2 = ft__strdup(src2)));
	free(dest);
	free(dest2);
}

void	test_ft__strcmp(void)
{
	char	str[] = "admf^s\t\ns\\0";
	char	str_e[] = "admf^s\t\ns\\0";
	char	str_n[] = "admf^s\t\ts\\0";
	char	str_p[] = "admf^s\t\vs\\0";
	
	printf("\n>>\tNow Testing\033[32;1m ft__strcmp()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	printf("%d\t|\t%d\n", strcmp(str, str_e), ft__strcmp(str, str_e));
	printf("%d\t|\t%d\n", strcmp(str, str_n), ft__strcmp(str, str_n));
	printf("%d\t|\t%d\n", strcmp(str, str_p), ft__strcmp(str, str_p));
}

void	test_ft__read(void)
{
	int		fd;
	char	buff_libc[128] = "";
	char	buff_libasm[128] = "";
	int		ret_libc;
	int		ret_libasm;
	ssize_t	num_bytes;

	printf("\n>>\tNow Testing\033[32;1m ft__read()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	fd = open("./srcs/ft__read.s", O_RDONLY);
	num_bytes = 127;
	ret_libc = read(fd, &buff_libc, num_bytes);
	close(fd);
	fd = open("./srcs/ft__read.s", O_RDONLY);
	ret_libasm = ft__read(fd, &buff_libasm, num_bytes);
	close(fd);
	printf("%d\t|\t%d\n", ret_libc, ret_libasm);
	printf("\n*****\033[32;1mlibc\033[0m*****\n");
	printf("%s\t\n", buff_libc);
	printf("\n*****\033[32;1mlibasm\033[0m*****\n");
	printf("%s\n", buff_libasm);
}

void	test_ft__write(void)
{
	char	c;

	printf("\n>>\tNow Testing\033[32;1m ft__write()\033[0m :\n");
	printf("\nlibc :\t|libasm :\n");
	c = 'a';
	write(1, &c, 1);
	ft_putstr_fd("\t| ", 1);
	ft__write(1,&c,1);
	ft_putstr_fd("\n", 1);
	c = 'A';
	write(1, &c, 1);
	ft_putstr_fd("\t| ", 1);
	ft__write(1,&c,1);
	ft_putstr_fd("\n", 1);
	c = '\t';
	write(1, &c, 1);
	ft_putstr_fd("\t| ", 1);
	ft__write(1,&c,1);
	ft_putstr_fd("\n", 1);
	c = '\0';
	write(1, &c, 1);
	ft_putstr_fd("\t| ", 1);
	ft__write(1,&c,1);
	ft_putstr_fd("\n", 1);
	c = -1;
	write(1, &c, 1);
	ft_putstr_fd("\t| ", 1);
	ft__write(1, &c,1);
	ft_putstr_fd("\n", 1);
}