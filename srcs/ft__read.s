section	.text
	extern	__errno_location
	global	ft__read

ft__read:
	xor	rax,rax					;syscall n°#0 -> read
	syscall						;do read
	cmp	rax,0					;test read return value
		jl _error				;if rax < 0, throw error
	ret							;return

_error:
	neg rax						;abs(rax) == error_no
	mov	rdx,rax					;save value
	call	__errno_location	;get errono ptr
	mov [rax],rdx				;fill ptr location with error_no
	mov rax,-1					;return (-1)
	ret							;return