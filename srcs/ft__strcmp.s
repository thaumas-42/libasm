section .text
	global ft__strcmp

ft__strcmp:
	xor rax,rax
	xor	cx,cx
	xor	dx,dx

_loop:
	mov cl,[rdi + rax]	
	mov dl,[rsi + rax]
	or	cl,cl
		jz _return
	or	dl,dl
		jz _return
	cmp cl,dl
		jne _return
	inc rax
	jmp	_loop

_return:
	cmp cx,dx
		jl _ret_neg
		ja _ret_pos
	mov rax, 0
	ret

_ret_neg:
	mov rax, -1
	ret

_ret_pos:
	mov rax, 1
	ret