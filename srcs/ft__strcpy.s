section .text
	global ft__strcpy

;dest = rdi
;src = rsi
;dl == char

ft__strcpy:
	xor rax, rax						;rax = 0

_loop:
	mov dl, [rsi + rax]					;dl = src[rax]
	or dl, dl							;if dl == 0
		jz _return						;then goto _return
	mov [rdi + rax], dl					;dest[rax] = dl
	inc rax								;rax++
	jmp _loop							;goto _loop

_return:
	mov BYTE[rdi + rax], 0x0			;dest[rax] = 0
	mov rax, rdi						;rax = dest
	ret									;return rax