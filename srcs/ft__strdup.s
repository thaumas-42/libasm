section .text
	global ft__strdup
	extern ft__strlen
	extern ft__strcpy
	extern malloc

ft__strdup:
	call ft__strlen
	inc rax
	push rdi
	mov rdi,rax
	call malloc
	pop rdi
	or rax,rax
		jz _error
	mov rsi,rdi
	mov rdi, rax
	call ft__strcpy
	ret

_error:
	xor rax,rax
	ret