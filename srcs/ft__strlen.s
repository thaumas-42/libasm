section .text
	global ft__strlen

ft__strlen:
	xor rax, rax				;set rax to 0
	jmp _while					;goto _while

_while:
	cmp BYTE[rdi + rax], 0x0	;if (*(rdi + rax) == 0 
		jz _return				;then goto _return
	inc rax						;rax++
	jmp _while					;goto _while

_return:
	ret							;return rax