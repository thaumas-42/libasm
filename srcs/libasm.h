/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thaumas <thaumas@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/29 19:15:26 by thaumas           #+#    #+#             */
/*   Updated: 2020/08/02 04:12:54 by thaumas          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_LIBASM_H
# define LIBASM_LIBASM_H
# include <strings.h>
# include <stdlib.h>

size_t	ft__strlen(char const *str);
int		ft__strcmp(char const *s1, char const *s2);
char	*ft__strcpy(char *dst, char const *src);
ssize_t	ft__write(int fd, void const *buf, size_t nbyte);
ssize_t	ft__read(int fd, void *buf, size_t nbyte);
char	*ft__strdup(char const *s1);
#endif
